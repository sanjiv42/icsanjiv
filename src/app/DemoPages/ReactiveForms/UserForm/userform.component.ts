import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { phoneNumberValidator } from './phone.validator';
import { AgeValidator } from './age.validator';

@Component({
    selector: 'user-form',
    templateUrl: './userform.component.html',
    styles: []
})
export class UserDataForm implements OnInit {
    @ViewChild("toaster") toaster: ElementRef;

    heading = 'User Data Form';
    subheading = 'Enter user details';
    icon = 'pe-7s-graph text-success';

    userForm: FormGroup;
    submitted = false;
    showToaster = false;

    constructor(private formBuilder: FormBuilder) {

    }


    ngOnInit() {
        this.userForm = this.formBuilder.group({
            Name: ['', Validators.required],
            age: ['', [Validators.required, AgeValidator]],
            email: ['', [Validators.required, Validators.email]],
            contact: ['', [Validators.required, phoneNumberValidator]]
        });
    }


    // convenience getter for easy access to form fields
    get f() { return this.userForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }

        this.showToaster = true;
        setTimeout(() => { this.toaster.nativeElement.classList.remove('show') }, 2000);
    }

}
