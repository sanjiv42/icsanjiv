import { AbstractControl } from '@angular/forms'

export function AgeValidator(
    control: AbstractControl
): { [key: string]: any } | null {
    const valid = control.value >= 18 && control.value <= 90;
    return (valid  || control.value == ""  || control.value == null)
        ? null
        : { invalidAge: { valid: false, value: control.value } }
}