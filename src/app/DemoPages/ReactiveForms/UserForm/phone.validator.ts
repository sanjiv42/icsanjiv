import { AbstractControl } from '@angular/forms'

export function phoneNumberValidator(
    control: AbstractControl
): { [key: string]: any } | null {
    const valid = /^\d+$/.test(control.value)
    return (valid || control.value == '')
        ? null
        : { invalidNumber: { valid: false, value: control.value } }
}